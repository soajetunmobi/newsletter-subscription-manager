Newsletter Subscription Manger
====================================

DESCRIPTION
-----------
NSM is a custom module to manage user's newsletter subscription.

FEATURES
-----------
- Provides users access to their newsletter subscription option and allows user to change it.

INSTALLATION
-----------
1) Copy nsm directory to site/all/modules/custom directory
2) Enable the module at: /admin/modules

Notes:
 - database table automatically creates itself once the module is enabled.