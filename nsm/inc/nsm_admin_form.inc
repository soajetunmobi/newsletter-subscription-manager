<?php

function nsm_admin_settings($form, &$form_state) {
    $form['description'] = array(
        '#type' => 'item',
        '#title' => t('Configure nsm module'),
    );

    $form['nsm_send_user_email'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send user confirmation email.'),
        '#default_value' => variable_get('nsm_send_user_email', 0),
    );

    $form['nsm_email_address_container'] = array(
        '#type' => 'container',
        '#states' => array(
            "visible" => array(
                "input[name='nsm_send_user_email']" => array("checked" => TRUE)),
        ),
    );

    $form['nsm_email_address_container']['nsm_from_email_address'] = array(
        '#type' => 'textfield',
        '#title' => t('Email Address to send from if different from site email.'),
        '#default_value' => variable_get('nsm_from_email_address', variable_get('site_mail', '')),
        '#attributes' => array('class' => array('nsm-from-email-address'), 'placeholder' => array('Email address *')),
        '#required' => (isset($form_state["values"]["nsm_send_user_email"]) ? ($form_state["values"]["nsm_send_user_email"] == true ? TRUE : FALSE) : FALSE),
    );

    $form['nsm_send_admin_email'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send admin after user submission.'),
        '#default_value' => variable_get('nsm_send_admin_email', 0),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
    );
    return $form;
}

/**
 * Adds a submit handler/function to our form to send a successful
 * completion message to the screen.
 */
function nsm_admin_settings_submit($form, &$form_state) {
    variable_set('nsm_send_user_email', $form_state['values']['nsm_send_user_email']);
    variable_set('nsm_from_email_address', $form_state['values']['nsm_from_email_address']);
    variable_set('nsm_send_admin_email', $form_state['values']['nsm_send_admin_email']);
    drupal_set_message(t('Settings have been saved.'));
}